# Telegram Concarnatio

A configurable software to run your Telegram bots. You can run 
multiple bots concurrently and configure them
 individually.
 
## Example config
The keys of 'responseMap' are regular expressions.
The regular expression that matches the received text and 
has the highest priority is chosen. If multiple responses are
 linked to a regular expression, a random one is chosen.
```
- username: "example1_bot"
  token: "adawo2in23o1!ni3"
  chatIds:
    - 202
    - 729382
  reactor:
    responseMap:
      ' A':
        priority: 600
        responses:
          - "bye"
      .*:
        priority: 300
        responses:
          - "hi"
          - "hallo"
- username: "example2_bot"
  token: "gkhjgdiawjdij2222"
  chatIds:
    - 377
  reactor:
    responseMap:
      ' ':
        priority: 600
        responses:
          - "ok"
```

## Authors

@devgioele

## License

[MIT](https://choosealicense.com/licenses/mit/)
