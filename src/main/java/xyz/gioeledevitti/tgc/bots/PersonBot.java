package xyz.gioeledevitti.tgc.bots;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

public class PersonBot extends TelegramLongPollingBot {

    private static final Logger logger = LogManager.getLogger();

    private final PersonBotCore core;

    public PersonBot(PersonBotCore core) {
        this.core = core;
    }

    public static PersonBot buildUsing(PersonBotCore core) {
        return new PersonBot(core);
    }

    public static List<PersonBot> buildUsing(List<PersonBotCore> cores) {
        List<PersonBot> pbs = new ArrayList<>();
        for (PersonBotCore core : cores)
            pbs.add(buildUsing(core));

        return pbs;
    }

    @Override
    public void onUpdateReceived(Update update) {
        logger.info("Processing new update.");
        if (update.hasMessage()) {
            Message msg = update.getMessage();
            logger.debug("ChatID: " + msg.getChatId());
            // Respond to some chats only or to everyone if no chats are given
            if (core.isChatIdValid(msg.getChatId())) {
                logger.debug("Looking for matching response.");
                try {
                    SendMessage reaction = core.getReactor().react(msg, false);
                    if (reaction != null) {
                        execute(reaction);
                        logger.info("Responded.");
                    } else
                        logger.debug("No matching response found.");
                } catch (TelegramApiException e) {
                    logger.error(e);
                }
            }
        }
    }

    @Override
    public String getBotUsername() {
        return core.getUsername();
    }

    @Override
    public String getBotToken() {
        return core.getToken();
    }

}
