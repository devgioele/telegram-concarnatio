package xyz.gioeledevitti.tgc.bots;

import xyz.gioeledevitti.tgc.io.ResourceFile;
import xyz.gioeledevitti.tgc.reactions.Reactor;

import java.util.Arrays;

public class PersonBotCore {

    private String username;
    private String token;
    private Long[] chatIds;
    private Reactor reactor;

    public PersonBotCore() { }

    public PersonBotCore(String username, String token, Long[] chatIds, Reactor reactor) {
        this.username = username;
        this.token = token;
        this.chatIds = chatIds;
        this.reactor = reactor;
    }

    public boolean isChatIdValid(Long id) {
        return chatIds.length == 0 ||
                Arrays.asList(chatIds).contains(id);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long[] getChatIds() {
        return chatIds;
    }

    public void setChatIds(Long[] chatIds) {
        this.chatIds = chatIds;
    }

    public Reactor getReactor() {
        return reactor;
    }

    public void setReactor(Reactor reactor) {
        this.reactor = reactor;
    }

}
