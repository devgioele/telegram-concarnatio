package xyz.gioeledevitti.tgc.utils;

public class Commons {

    public static int bitmask(int... flags) {
        int result = 0;
        for(int flag : flags)
            result |= flag;

        return result;
    }

}
