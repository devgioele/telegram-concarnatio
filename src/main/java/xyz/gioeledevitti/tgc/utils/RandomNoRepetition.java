package xyz.gioeledevitti.tgc.utils;

import java.util.List;
import java.util.Random;

public class RandomNoRepetition {

    private final Random rnd = new Random();

    private int lastIndex = -1;

    public <T> T next(List<T> list) {
        // Ensure that the new index is random, but is not the same as the previous one
        int index = lastIndex;
        while (index == lastIndex) {
            index = rnd.nextInt(list.size());
            if (list.size() == 1) break;
        }
        lastIndex = index;

        // Return element using random index
        return list.get(index);
    }

    public <T> T next(T[] list) {
        // Ensure that the new index is random, but is not the same as the previous one
        int index = lastIndex;
        while (index == lastIndex) {
            index = rnd.nextInt(list.length);
            if (list.length == 1) break;
        }
        lastIndex = index;

        // Return element using random index
        return list[index];
    }

}
