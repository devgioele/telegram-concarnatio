package xyz.gioeledevitti.tgc.reactions;

import xyz.gioeledevitti.tgc.utils.Commons;
import xyz.gioeledevitti.tgc.utils.RandomNoRepetition;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ResponseMap extends HashMap<String, Response> {

    private final RandomNoRepetition rnd = new RandomNoRepetition();

    public Response find(String input) {
        // Find all the responses to which the input matches
        List<Response> matchingResponses = keySet().parallelStream()
                .filter(regex -> matches(input, regex, Pattern.CASE_INSENSITIVE)) // Keep only regexes that match
                .map(this::get) // Use those keys to get the corresponding value
                .collect(Collectors.toList());

        // Find the response with the highest priority
        Integer highestPriority = matchingResponses.parallelStream()
                .map(Response::getPriority)
                .max(Comparator.comparing(Integer::valueOf))
                .orElse(null);

        // If a response exists
        if (highestPriority != null) {
            // Reduce the responses to those of the highest priority only
            matchingResponses = matchingResponses.parallelStream()
                    .filter(response -> response.getPriority() == highestPriority)
                    .collect(Collectors.toList());

            // Choose a random response
            return rnd.next(matchingResponses);
        }

        return null;
    }

    private boolean matches(String input, String regex, int... patterns) {
        Pattern pattern = Pattern.compile(regex, Commons.bitmask(patterns));
        Matcher matcher = pattern.matcher(input);
        return matcher.matches();
    }

}
