package xyz.gioeledevitti.tgc.reactions;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

public class Reactor {

    private ResponseMap responseMap = new ResponseMap();

    public SendMessage react(Message msg, boolean replyDirectly) {
        Response response = null;

        if (msg.hasText()) {
            String receivedTxt = msg.getText();
            response = responseMap.find(receivedTxt);
        }

        if (response == null)
            return null;

        SendMessage reaction = new SendMessage()
                .setChatId(msg.getChatId()) // Reply in the same chat
                .setText(response.get());
        if(replyDirectly)
            reaction.setReplyToMessageId(msg.getMessageId());
        return reaction;
    }

    public ResponseMap getResponseMap() {
        return responseMap;
    }

    public void setResponseMap(ResponseMap responseMap) {
        this.responseMap = responseMap;
    }

}
