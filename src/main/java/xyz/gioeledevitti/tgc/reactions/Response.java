package xyz.gioeledevitti.tgc.reactions;

import xyz.gioeledevitti.tgc.utils.RandomNoRepetition;

public class Response {

    private final RandomNoRepetition rnd = new RandomNoRepetition();

    private int priority;
    private String[] responses;

    public Response() {
    }

    public Response(int priority, String... responses) {
        this.priority = priority;
        this.responses = responses;
    }

    public String get() {
        return rnd.next(responses);
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String[] getResponses() {
        return responses;
    }

    public void setResponses(String[] responses) {
        this.responses = responses;
    }

}
