package xyz.gioeledevitti.tgc.io;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * {@code ResourceFile} enumerates resource files, verifying their existence and providing instances
 * of {@link InputStreamReader} to access them.
 * ResourceFile files are files inside the 'resources' directory, which is the root directory of a packaged JAR file.
 *
 * @author Gioele De Vitti
 */
public enum ResourceFile {
    PERSON_BOTS("person bots.yaml", "person bots.yaml", "person bots.url");

    private static final Logger logger = LogManager.getLogger();

    private final String internalFilepath;
    private String externalFilepath;
    private String urlFilepath;

    /**
     * Creates a new instance of {@link ResourceFile}.
     *
     * @param internalFilepath The internal filepath to the resource file, which is a relative path.
     *                         The root directory is the 'resources' directory.
     * @param externalFilepath The external filepath to the resource file.
     *                         If this filepath is specified, {@link #open()} first attempts using this filepath.
     *                         If it fails, it exports the default file located at {@link #internalFilepath} to
     *                         {@link #externalFilepath}.
     * @param urlFilepath      An external file that contains the url from where the resource file should be retrieved.
     */
    ResourceFile(String internalFilepath, String externalFilepath, String urlFilepath) {
        this.internalFilepath = "/" + internalFilepath;
        this.externalFilepath = externalFilepath;
        this.urlFilepath = urlFilepath;
    }

    /**
     * Creates a new instance of {@link ResourceFile}.
     *
     * @param internalFilepath The internal filepath to the resource file, which is a relative path.
     *                         This path must begin with a / and its root directory is the 'resources' directory.
     * @param externalFilepath The external filepath to the resource file.
     *                         If this filepath is specified, {@link #open()} first attempts using this filepath.
     *                         If it fails, it exports the default file located at {@link #internalFilepath} to
     *                         {@link #externalFilepath}.
     */
    ResourceFile(String internalFilepath, String externalFilepath) {
        this.internalFilepath = internalFilepath;
        this.externalFilepath = externalFilepath;
    }

    /**
     * Creates a new instance of {@link ResourceFile}, without external filepath.
     *
     * @param internalFilepath The relative file path to the resource file.
     *                         This path must begin with a / and its root directory is the 'resources' directory.
     */
    ResourceFile(String internalFilepath) {
        this.internalFilepath = internalFilepath;
    }

    /**
     * Returns the internal filepath of this resource file.
     *
     * @return The internal filepath of this resource file.
     */
    public String getInternalFilepath() {
        return internalFilepath;
    }

    /**
     * Returns the external filepath of this resource file.
     *
     * @return The external filepath of this resource file.
     */
    public String getExternalFilepath() {
        return externalFilepath;
    }

    /**
     * Opens this resource file with a {@link InputStreamReader}.
     *
     * @return The resource as a new {@link InputStreamReader}.
     */
    public InputStreamReader open() {
        InputStreamReader reader = null;
        // Download online file if specified
        if (urlFilepath != null) {
            download(externalFilepath);
        }
        // Load external file if specified
        if (externalFilepath != null) {
            reader = openExternal();
        }
        if (reader == null) {
            exportDefault();
            return openInternalAsReader();
        }
        return reader;
    }

    private void download(String destinationFilepath) {
        logger.debug("Downloading file.");

        // Extract url from url-file
        String urlRaw;
        try {
            urlRaw = FileUtils.readFileToString(new File(urlFilepath), StandardCharsets.UTF_8);
        } catch (IOException e) {
            logger.warn(e);
            return;
        }

        // Verify url
        URL url;
        try {
            url = new URL(urlRaw);
        } catch (MalformedURLException e) {
            logger.warn(e);
            return;
        }

        // Download content
        String content;
        try {
            content = IOUtils.toString(url, StandardCharsets.UTF_8);
        } catch (IOException e) {
            logger.warn(e);
            return;
        }

        // Save to external filepath
        try(FileWriter writer = new FileWriter(destinationFilepath)) {
            writer.write(content);
        } catch (IOException e) {
            logger.warn(e);
        }
    }

    private FileReader openExternal() {
        logger.debug("Opening external file.");
        try {
            return new FileReader(externalFilepath);
        } catch (FileNotFoundException e) {
            logger.warn("Could not find config file '" + externalFilepath + "'.", e);
            return null;
        }
    }

    /**
     * Opens the internal resource file with a {@link InputStreamReader}.
     *
     * @return The internal resource as a new {@link InputStreamReader}.
     */
    private InputStreamReader openInternalAsReader() {
        return new InputStreamReader(openInternal());
    }

    private InputStream openInternal() {
        return getClass().getResourceAsStream(internalFilepath);
    }

    private void exportDefault() {
        // Export internal file to external filepath
        logger.info("Exporting default config file.");
        try {
            Files.copy(openInternal(), Paths.get(externalFilepath));
        } catch (IOException e) {
            logger.warn("Could not export internal resource '" + internalFilepath + "'.");
        }
    }

}
