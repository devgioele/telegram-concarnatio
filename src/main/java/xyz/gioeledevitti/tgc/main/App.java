package xyz.gioeledevitti.tgc.main;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.generics.BotSession;
import xyz.gioeledevitti.tgc.bots.PersonBot;
import xyz.gioeledevitti.tgc.bots.PersonBotCore;
import xyz.gioeledevitti.tgc.constants.Command;
import xyz.gioeledevitti.tgc.exceptions.UncaughtExceptionHandler;
import xyz.gioeledevitti.tgc.io.ResourceFile;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class App {

    private static final Logger logger = LogManager.getLogger();

    private static final ObjectMapper mapper =
            new ObjectMapper(new YAMLFactory()
                    .disable(YAMLGenerator.Feature.WRITE_DOC_START_MARKER));
    private static final List<BotSession> sessions = new ArrayList<>();
    private static List<PersonBot> personBots = new ArrayList<>();

    public static void main(String[] args) {
        boot();
        loadConfig();
        BotHandler botHandler = new BotHandler(personBots, sessions);
        new Thread(botHandler).start();
        startCmdListener();
        shutdown(0);
    }

    private static void startCmdListener() {
        Scanner sc = new Scanner(System.in);
        boolean quit = false;

        logger.info("Listening to commands.");
        while (!quit) {
            String entry = sc.next();
            logger.info("Processing entry: {}.", () -> entry);
            Command cmd;
            try {
                cmd = Enum.valueOf(Command.class, entry.toUpperCase());
            } catch (IllegalArgumentException e) {
                logger.info("Unknown command. Available commands: {}", Arrays.toString(Command.values()));
                continue;
            }
            quit = processCmd(cmd);
        }
    }

    private static boolean processCmd(Command cmd) {
        return switch (cmd) {
            case QUIT -> true;
            case RELOAD -> {
                logger.info("Reload function is yet to be implemented.");
                yield false;
            }
        };
    }

    private static void boot() {
        logger.info("----------------------------");
        logger.info("Booting.");

        Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler());

        // Load Jackson's modules once for all, because it is expensive
        logger.debug("Loading Jackson's modules.");
        mapper.findAndRegisterModules();

        logger.debug("Initializing TelegramBotsApi.");
        ApiContextInitializer.init();
    }

    public static void shutdown(int status) {
        logger.info("Shutting down.");

        logger.debug("Stopping bots.");
        for (BotSession session : sessions)
            session.stop();
        logger.debug("Bots stopped.");

        System.exit(status);
    }

    private static void loadConfig() {
        Object obj;
        List<PersonBotCore> pbCores = new ArrayList<>();

        logger.info("Loading configuration.");
        try (InputStreamReader reader = ResourceFile.PERSON_BOTS.open()) {
            obj = mapper.readValue(reader, pbCores.getClass());
        } catch (MismatchedInputException e) {
            logger.warn("Could not find any bot data inside the config file.", e);
            return;
        } catch (IOException e) {
            logger.error(e);
            return;
        }
        pbCores = mapper.convertValue(obj, new TypeReference<>() {
        });
        personBots = PersonBot.buildUsing(pbCores);
    }

}
