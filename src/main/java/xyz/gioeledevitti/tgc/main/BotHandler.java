package xyz.gioeledevitti.tgc.main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;
import org.telegram.telegrambots.meta.generics.BotSession;
import xyz.gioeledevitti.tgc.bots.PersonBot;

import java.util.List;

public class BotHandler implements Runnable {

    private static final Logger logger = LogManager.getLogger();

    private final List<PersonBot> personBots;
    private final List<BotSession> sessions;

    public BotHandler(List<PersonBot> personBots, List<BotSession> sessions) {
        this.personBots = personBots;
        this.sessions = sessions;
    }

    @Override
    public void run() {
        logger.info("Registering {} bots.", personBots.size());
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        try {
            for (PersonBot pb : personBots)
                sessions.add(telegramBotsApi.registerBot(pb));
        } catch (TelegramApiRequestException e) {
            logger.error(e);
            App.shutdown(1);
        }
    }

}
